class Calculator
  class Rate
    class << self
      def calc month_count, pmt, total_sum
        Exonio.rate(month_count, pmt, -total_sum) * 12
      end
    end
  end
end