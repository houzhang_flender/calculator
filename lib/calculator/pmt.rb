class Calculator
  class Pmt
    class << self
      def calc month_rate, month_count, total_sum
        - Exonio.pmt(month_rate / 12, month_count, total_sum)
      end
    end
  end
end