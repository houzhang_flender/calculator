class Calculator
  class Apr
    class << self
      def calc month_rate, month_count
        (1 + month_rate * 1.0 / month_count) ** month_count - 1
      end

      ##
      # Calculate APR, given:
      #
      # * cash_flow_array - A sequence of cash flows
      # * cppy - The number of compounding periods per year
      #
      def calc_eir(cash_flow_array, cppy = 12)
        irr = (cash_flow_array.irr * cppy).to_f.round(6)
        effective_interest_rate(irr, cppy)
      end

      ##
      # Determines the effective interest rate, given:
      #
      # * nair - The nominal annual interest rate
      # * cppy - The number of compounding periods per year
      #
      def effective_interest_rate(nair, cppy = 12)
        (((nair / cppy) + 1) ** cppy) - 1
      end
    end
  end

end