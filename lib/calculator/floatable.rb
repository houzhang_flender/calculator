class Calculator
  class Floatable
    class << self
      def ceil_to(num, point = 0)
        (num * 10**point).ceil.to_f / 10**point
      end
    end
  end
end